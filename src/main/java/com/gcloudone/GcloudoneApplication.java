package com.gcloudone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GcloudoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(GcloudoneApplication.class, args);
	}

}
