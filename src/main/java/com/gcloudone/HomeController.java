package com.gcloudone;

import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@GetMapping("/")
	public String home(){
		
		return "HOME PAGE"+new Date();
		
	}
	
	@GetMapping("/home1")
	public String home1(){
		
		return "HOME PAGE one "+new Date();
		
	}
	@GetMapping("/home2")
	public String home2(){
		
		return "HOME PAGE two"+new Date();
		
	}
}
