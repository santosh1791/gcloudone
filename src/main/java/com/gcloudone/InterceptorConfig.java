package com.gcloudone;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
	public class InterceptorConfig implements WebMvcConfigurer {
	   //@Autowired
	   //RequestProcessingTimeInterceptor requestProcessingTimeInterceptor;

	   @Override
	   public void addInterceptors(InterceptorRegistry registry) {
		   
		   RequestProcessingTimeInterceptor requestProcessingTimeInterceptor = new RequestProcessingTimeInterceptor();
	      registry.addInterceptor(requestProcessingTimeInterceptor).addPathPatterns("/home1","/home2","/api/module1*");
	   }
	}