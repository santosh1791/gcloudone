package com.gcloudone;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
@Configuration
public class ViewResolverConfig {
	
	
	@Bean
	public ViewResolver internalResourceViewResolverOne() {
	    InternalResourceViewResolver bean = new InternalResourceViewResolver();
	    //bean.setViewClass(JstlView.class);
	    bean.setPrefix("/");
	    bean.setSuffix(".jsp");
	    bean.setOrder(1);
	    //bean.setPrefix(".jsp");
	    return bean;
	}
	@Bean
	public ViewResolver internalResourceViewResolver() {
		ResourceBundleViewResolver bean = new ResourceBundleViewResolver();
	    //bean.setViewClass(JstlView.class);
	    //bean.setPrefix("/");
	    //bean.setSuffix(".html");
		bean.setBasename("views");
	    bean.setOrder(0);
	    //bean.setPrefix(".jsp");
	    return bean;
	}
}
