package com.gcloudone;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {
	@RequestMapping("/m1")
	public String m1() {
		System.out.println("In m1");
		return "page1";
	}
	
	@RequestMapping("/m2")
	public String m2() {
		System.out.println("In m2");
		return "page2"; 
	}
	
	
}
